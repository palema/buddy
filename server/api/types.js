
const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLNonNull,
    GraphQLList,
    GraphQLInt,
    GraphQLBoolean
} = require('graphql')

const UserType = new GraphQLObjectType({
    name: 'UserType',
    fields: () => ({
        username: { type: GraphQLString }
    })
})

const MessageType = new GraphQLObjectType({
    name: 'MessageType',
    fields: () => ({
        message: { type: GraphQLString },
        replyTo: { type: GraphQLString },
        to: { type: GraphQLString },
        from: { type: GraphQLString },
        viewed: { type: GraphQLBoolean },
        time: { type: GraphQLInt }
    })
})


const MessageContainer = new GraphQLObjectType({
    name: 'Messag',
    fields: () => ({
        _id: { type: GraphQLString },
        unread: { type: GraphQLInt },
        messages: { type: GraphQLList(MessageType) }
    })
})

const AuthPayload = new GraphQLObjectType({
    name: 'AuthenticationPayload',
    fields: () => ({
        token: { type: GraphQLString },
        error: { type: GraphQLString }
    })
})

module.exports = { UserType, MessageType, AuthPayload, MessageContainer }