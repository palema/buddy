const jwt = require('jsonwebtoken')
if(process.env.development){
    require('dotenv').config()
}


function setToken(username) {
    const token = jwt.sign(
        { username },
        process.env.JWT_PRIVATE_KEY,
        { expiresIn: '7d' }
    )

    return token
}

function checkToken(token){
	return jwt.verify(token, process.env.JWT_PRIVATE_KEY)
}

module.exports = { setToken, checkToken }