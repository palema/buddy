const {
    GraphQLSchema,
    GraphQLObjectType,
    GraphQLString,
    GraphQLNonNull,
    GraphQLList,
    GraphQLInt,
    GraphQLBoolean
} = require('graphql')

const { 
    UserType, 
    MessageType, 
    AuthPayload, 
    MessageContainer 
} = require('./types')

const { 
    findUnread, 
    findAllMessages 
} = require('../filter')

const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: () => ({
        login: {
            type: AuthPayload,
            args: {
                username: { type: GraphQLNonNull(GraphQLString) },
                password: { type: GraphQLNonNull(GraphQLString) }
            },
            resolve(_, args, { auth, bcrypt, User }) {
                return User.findOne({ username: args.username }, { password: 1, _id: 0 })
                    .then((doc) => {
                        if (!doc) return { error: 'incorrect credentials', token: null }

                        return bcrypt.compare(args.password, doc.password)
                            .then((result) => {
                                if (!result) return { error: 'incorrect credentials', token: null }

                                return { error: null, token: auth.setToken(args.username) }
                            })
                    })
                    .catch(error => {
                        return error
                    })
            }
        },
        discover: {
            type: GraphQLList(UserType),
            args: {
                queryString: { type: GraphQLString }
            },
            resolve(_, args, { User }) {
                return User.find({ username: { $regex: args.queryString, $options: 'i' } })
                .then(docs=>{
                    return docs
                })
            }
        },
        reply: {
            type: MessageType,
            args: {
                message: { type: GraphQLString },
                replyTo: { type: GraphQLString },
                to: { type: GraphQLString },
                from: { type: GraphQLString },
                viewed: { type: GraphQLBoolean },
                time: { type: GraphQLInt }
            },
            resolve(_, args, { MessageSchema }) {
                var m = new MessageSchema({
                    message: args.message,
                    replyTo: args.replyTo,
                    to: args.to,
                    from: args.from,
                    viewed: args.viewed,
                    time: args.time
                })
                return m.save()
                    .then(doc => {
                        console.log(doc)
                        return doc
                    })
            }
        },
        find: {
            type: GraphQLList(MessageContainer),
            args: {
                user: { type: GraphQLString }
            },
            resolve(_, args, { MessageSchema}) {
                MessageSchema.find({}, (err, doc) => {
                    console.log(doc)
                })
                return findUnread(args.user)
                    .then(docs => {
                        console.log(docs)
                        return docs
                    })
            }

        },
        findAll: {
            type: GraphQLList(MessageContainer),
            args: {
                user: { type: GraphQLString }
            },
            resolve(_, args) {
                return findAllMessages(args.user)
                    .then(docs => {
                        return docs
                    })
            }

        }
    })
})

const RooMutation = new GraphQLObjectType({
    name: 'RootMutationType',
    fields: () => ({
        register: {
            type: AuthPayload,
            args: {
                username: { type: GraphQLNonNull(GraphQLString) },
                password: { type: GraphQLNonNull(GraphQLString) }
            },
            resolve: (_, args, { auth, User, bcrypt }) => {
                return bcrypt.hash(args.password, 12)
                    .then(hash => {
                        const user = new User({
                            username: args.username,
                            password: hash
                        })

                        return user.save()

                    })
                    .then(doc => {
                        return { error: null, token: auth.setToken(doc.username)}
                    })
                    .catch(err => {
                        console.log(err, ' server err')
                        if(err.code === 11000)
                            return { error: ' username exists', token: null }

                        return { error, token : null }
                    })
            }
        }
    })
})



const schema = new GraphQLSchema({
    query: RootQuery,
    mutation: RooMutation
})

module.exports = schema


