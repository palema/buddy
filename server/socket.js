const MessageSchema = require('./models/message.model')
const auth = require('./api/auth')

const User = require('./models/user.model')
const { findUnread, findAllMessages } = require('./filter')
module.exports = {

    authenticate: (socket, token, callback) => {
        if (!token) {
            return callback(false, new Error('not authenticated'))
        }

        console.log('user authenticating ')

        //get credentials sent by the client
        try {

            var decoded = auth.checkToken(token)
            var username = decoded.username;
            socket.client.user = username;
            socket.join(username)
            return callback(true, 'authentication successful');
        } catch (e) {
            //inform the callback of auth success/failure
            return callback(false, new Error('invalid token'));
        }
    },
    socketConfig: (socket) => {

        socket.on('getAllMessages', () => {
            findAllMessages(socket.client.user)
                .then(docs => {
                    socket.emit("allMessages", docs)
                })

        })

        /**
         * handles message transmission between socket rooms
         */
        socket.on('sendMessage', (msg) => {

            //create message object to send to database server
            var message = new MessageSchema({
                from: socket.client.user,
                to: msg.to,
                message: msg.message,
                time: msg.time,
                viewed: msg.viewed
            })

            //save document to database
            message.save().then(result => {
                console.log(result)
            })

            //only send to message recipient
            socket.to(msg.to).emit('newMessage', msg)

        })


        socket.on('request', (data) => {
            User.startSession()
                .then(_session => {
                    session = _session
                    session.startTransaction()
                    return User.update({ username: data.from },
                        {
                            $addToSet: {
                                requestSent: data.to
                            }
                        }, { session })
                })
                .then((res) => {
                    return User.update({ username: data.to },
                        {
                            $addToSet: {
                                requestReceived: data.from
                            }
                        }, { session }
                    )
                })
                .then(()=>{
                    return session.commitTransaction()
                })
                .then(()=>{
                    return session.endSession()
                })
                .then(()=>{
                    //foward request to recipient
                    socket.to(data.to).emit('notifyRequest', data.from)
                    //give feedback to sender that request has been sent
                    socket.emit('requestFeedback', data.to)
                })
                .catch((err)=>{
                    session.abortTransaction()
                })

        })

        socket.on('response', (data) => {
            var session = null
            User.startSession()
                .then(_session => {
                    session = _session
                    session.startTransaction()
                    if(data.accept){
                        return User.update(
                            { username: socket.client.user },
                            {
                                $push: {
                                    contacts: data.username
                                },
                                $pull: {
                                    requestReceived: data.username
                                }
                            }, { session })
                    }else{
                        return User.update(
                            { username: socket.client.user },
                            {
                                $pull: {
                                    requestReceived: data.username
                                }
                            }, { session })
                    }
                })
                .then(() => {
                    if(data.accept){
                        return User.update(
                            { username: data.username },
                            {
                                $push: {
                                    contacts: socket.client.user
                                },
                                $pull: {
                                    requestSent: socket.client.user
                                }
                            }, { session })
                    }
                    else{
                        return User.update(
                            { username: data.username },
                            {
                                $pull: {
                                    requestSent: socket.client.user
                                }
                            }, { session })
                    }
                })
                .then(()=>{
                    return session.commitTransaction()
                })
                .then(()=>{
                    return session.endSession()
                })
                .then(()=>{
                    var username = data.username
                    data.username = socket.client.user
                    socket.to(username).emit('notifyResponse', data);
                })
                .catch((err)=>{
                    session.abortTransaction()
                })
        })

        socket.on('getAcquaintances',()=>{
            User.findOne({ username: socket.client.user }, { _id: 0, requestReceived: 1, requestSent: 1 })
            .then(doc=>{
                socket.emit('acquaintances', doc)
            })
        })

        socket.on('getContacts',()=>{
            User.findOne({ username: socket.client.user }, { _id: 0, contacts: 1 })
            .then(doc=>{
                socket.emit('contacts', doc)
            })
        })

        socket.on('disconnect', () => {
            console.error(socket.client.user, ' disconnected')
        })
        socket.on('connect', () => {
            console.log(socket.client.user, ' connected')
        })

    }
}

