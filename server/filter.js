const MessageSchema = require('./models/message.model')


const findUnread = (username) => {
    return MessageSchema.aggregate([
        {
            $match: {
                $and: [
                    { viewed: false },
                    { to: username }
                ]
            }
        },
        {
            $group: {
                _id: "$from",
                unread: { $sum: 1 },
                messages: { $push: { message: "$message", time: "$time" } }
            }
        }
    ])
}

const findAllMessages = (username) => {
    return MessageSchema.find({$or: [
        { to: username },
        { from: username }
    ]}, { _id: 0 })
}

module.exports = { findUnread, findAllMessages }
