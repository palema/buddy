const { Schema, model, Model } = require('mongoose')


const messageSchema = new Schema({
    from: { type: String, required: true },
    to: { type: String, required: true },
    replyTo: { type: String },
    message: { type: String, required: true },
    viewed: { type: Boolean, default: false },
    recieved: { type: Boolean, default: false },
    time: { type: Number, required: true}
})

const schema = new model('Message', messageSchema)
module.exports = schema