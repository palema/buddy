const { Schema, model } = require('mongoose')

const user_schema = new Schema({
    username : { type: String, required: true, unique: true },
    password : { type: String, required: true },
    contacts: [String],
    requestSent: [String],
    requestReceived: [String]
})

module.exports = model('User', user_schema)
