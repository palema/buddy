const path = require('path')
const express = require('express')
const app = express()

const http = require('http').Server(app)

const graphql = require('express-graphql')
const schema = require('./api/schema')
const mongoose = require('mongoose')
const io = require('socket.io')(http)
const auth = require('./api/auth')
const cookieParser = require('cookie-parser')

const socketModule = require('./socket')

const bcrypt = require('bcrypt')
const User = require('./models/user.model')
const MessageSchema = require('./models/message.model')

//only use dotenv library for development
if(app.get('env') === 'development'){
    require('dotenv').config()
    
    const cors = require('cors')

    app.use(cookieParser())
    app.use(cors({ origin: [process.env.CLIENT_URL] }))

}else if (app.get('env') === 'production'){
    
    const forceSSL = function() { 
        return function(req, res, next) {
            if (req.headers['x-forwarded-proto'] !== 'https') {
                return res.redirect(
                    ['https://', req.get('Host'), req.url].join('')
                );
            }
            next();
        }
    }

    app.use(forceSSL())

}

const db_conn = mongoose.connect(
    process.env.DB_URL || process.env.MONGODB_URI, 
    { 
        useNewUrlParser: true, 
        useUnifiedTopology: true 
    })



//sets up view engine (ejs) which handles app rendering
app.set('views', __dirname + '../dist/buddy')
app.set('view engine', 'ejs')
app.engine('html', require('ejs').renderFile)

//state which directory is for static files
app.use(express.static(path.join(__dirname, '../dist/buddy')))


require('socketio-auth')(io, {
    authenticate: socketModule.authenticate,
    postAuthenticate: (socket, data)=>{
        console.log(socket.auth, ' is user authicated')
    }
})


app.use('/api', graphql((req, res) => ({
    schema,
    graphiql: (app.get('env') === 'development'),
    context: { req, res, auth, bcrypt, User, MessageSchema }
})))

/**
 * for routes not matching api send the app
 */
app.get('*', function (req, res)
{
    res.render(path.join(__dirname, '../dist/buddy/index.html'))
})


//setup socket.io
io.on('connection', socket => {
    socketModule.socketConfig(socket)
})




http.listen(process.env.PORT, () => {
    console.log('listesting on port ',process.env.PORT)
})
