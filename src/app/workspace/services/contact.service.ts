import { Injectable } from '@angular/core';
import gql from 'graphql-tag';
import { Apollo } from 'apollo-angular';
import { NgxIndexedDBService } from 'ngx-indexed-db';

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  public searchResults = []
  public loading

  constructor(private apollo: Apollo,
    private dbService: NgxIndexedDBService
  ) { }

  private contactSearchQ = gql`
    query Discovery($queryString: String){
      discover(queryString: $queryString){
        username
      }
    }
  `

  findContacts(queryString: string) {
    return this.apollo.watchQuery<any>({
      query: this.contactSearchQ,
      variables: {
        queryString
      }
    }).valueChanges
  }

  updateRequests(requests) {
    for (var i = 0; i < requests.length; i++) {
      this.dbService.update('acquaintances', requests[i])
        .then(ev => {
          console.log(ev, ' request stored')
        }, error => {
          console.error('Error something', error)
        })
    }
  }

  updateContacts(contacts) {
    for (var i = 0; i < contacts.length; i++) {
      this.dbService.update('contacts', { username: contacts[i] })
        .then(ev => {
          console.log(ev, ' contact stored')
        }, error => {
          console.error('Error something', error)
        })
    }
  }

  /**
   * queries indexdb for user contacts
   * @param contact contact bieng searched for
   */
  public findAllLocal(store: string) {
      return this.dbService.getAll<string[]>(store)
        .then((contacts: any) => {
          return contacts
        })
  }

  public findByLocal(store: string, contact: string){
    this.searchResults = []
      this.dbService.openCursorByIndex(store, 'username', IDBKeyRange.bound(contact, contact+'\uffff'), (evt) => {
        this.loading = false
        var cursor = (<any>evt.target).result;
        if (cursor) {
          this.searchResults.push(cursor.value)
          cursor.continue()
        }
      })
  }

  public fullSearchLocal(store: string, username: string){
    return this.dbService.getByIndex(store, 'username', username)
    .then(res=>{
      return res
    })
  }

  removeFromAcquaintanceStore(username) {


    this.dbService.openCursorByIndex('acquaintances', 'username', IDBKeyRange.only(username), (evt) => {
      var cursor = (<any>evt.target).result;
      if (cursor) {
        this.dbService.delete('acquaintances', cursor.value.id)
          .then(res => {
            console.log('removal status ', res)
          })
        cursor.continue();
      }
    })
  }




}



