import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { IMessage, IMessageList } from 'src/app/shared/interfaces'

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  public messages = this.socket.fromEvent<IMessageList>('allMessages')
  public newMessage = this.socket.fromEvent<IMessage>('newMessage')
  public unathorized = this.socket.fromEvent('unauthorized')
  public authenticated = this.socket.fromEvent('authenticated')
  public requests = this.socket.fromEvent('notifyRequest')
  public responses = this.socket.fromEvent('notifyResponse')
  public contacts = this.socket.fromEvent('contacts')
  public acquaintances = this.socket.fromEvent('acquaintances')
  
  constructor(
    private socket: Socket 
  ) { }

  public sendMessage(message: IMessage) {
    this.socket.emit('sendMessage', message);
  }

  public authenticate(token){
    this.socket.emit('authentication', token)
  }

  public allMessages(){
    this.socket.emit('getAllMessages')
  }

  public sendResponse(response){
    this.socket.emit('response', response)
  }

  public sendRequest(request){
    this.socket.emit('request', request)
  }

  public cancelRequest(request){
    this.socket.emit('cancelRequest', request)
  }

  public getContacts(){
    this.socket.emit('getContacts')
  }

  public getAcquaintances(){
    this.socket.emit('getAcquaintances')
  }
  
}


