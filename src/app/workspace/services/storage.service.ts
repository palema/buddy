import { Injectable } from '@angular/core';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { GlobalService } from 'src/app/core/services/global.service';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  public conversation: any = []
  public chats: any = []
  public routeId

  constructor(
    private dbService: NgxIndexedDBService,
    private global: GlobalService
  ) { }

    /**
     * filters group or user's messages exchanged between client and that user
     * @param user user's texts to look for
     * 
     */
  getConversation(user: string) {
    this.dbService.openCursorByIndex('messages', 'username', IDBKeyRange.only(user), (evt) => {
      var cursor = (<any>evt.target).result
      if(cursor){
        this.conversation.push(cursor.value)
        cursor.continue()
      }
    })

  }

  /**
   * filters through all conversations and
   * returns unique users and the last message
   */
  chatsAndLastText() {
    this.chats = []
    this.dbService.openCursor('chats', (e)=>{
      var cursor = (<any>e.target).result
      if(cursor){
        this.chats.push(cursor.value)
        cursor.continue()
      }
    })
  }

  /**
   * 
   * @param messages messages from server to add to local DB
   */
  updateLocalMessages(messages) {
    messages.forEach(async msg => {
      //set username field to other user's name, for search purposes
      if(msg.to === this.global.activeUser){
        msg.username = msg.from
      }else{
        msg.username = msg.to
      }

      if(this.global.connectionStatus){
        await this.dbService.add('messages', msg).then(e=>{
          console.log(e, ' message stored')
        })
      }else{
        await this.dbService.add('offlineMessages', msg).then(e=>{
          console.log(e, 'offline message stored')
        })
      }
    })
  }

  

  updateChats(message, username){

    this.dbService.getByIndex('chats', 'username', username)
    .then(res=>{
      if(res){
        res.message = message
        this.dbService.update('chats', res)
      }else{
        this.dbService.add('chats', { message, username })
      }
    })
  }

}
``