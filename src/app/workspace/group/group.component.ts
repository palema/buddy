import { Component, OnInit } from '@angular/core'
import { StorageService } from '../services/storage.service'

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.scss']
})
export class GroupComponent implements OnInit {

  constructor(
    private storage: StorageService
  ) { }

  ngOnInit() {
    this.storage.chatsAndLastText()
  }

  get chats(){
    return this.storage.chats
  }





}
