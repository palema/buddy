import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WorkspaceComponent } from './workspace.component';
import { GroupComponent } from './group/group.component';
import { ChatComponent } from './chat/chat.component';
import { DiscoveryComponent } from './discovery/discovery.component';


const routes: Routes = [
  { path:'', component: WorkspaceComponent, children: [
    { path: 'chats', component: GroupComponent, data: { animation: 'chats' } },
    { path: 'chat/:id', component: ChatComponent, data: { animation: 'chat' } },
    { path: 'discover', component: DiscoveryComponent, data: { animation: 'discover'}},
    { path: '', redirectTo: 'chats', pathMatch: 'full' }
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkspaceRoutingModule { }
