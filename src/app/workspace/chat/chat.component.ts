import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { ActivatedRoute } from '@angular/router';
import { SocketService } from '../services/socket.service';
import { GlobalService } from 'src/app/core/services/global.service';
import { StorageService } from '../services/storage.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {
  public chatName: string

  constructor(
    private route: ActivatedRoute,
    private socket: SocketService,
    private global: GlobalService,
    private storage: StorageService,
  ) { }

  ngOnInit() {
    this.chatName = this.route.snapshot.paramMap.get('id')
    this.storage.routeId = this.chatName
    this.storage.conversation = []
    this.storage.getConversation(this.chatName)
  }

  /**
   * prevents consercutive display of sender's icon
   * @param i index of text
   */
  showIcon(i){
    if(i>0){
      if(this.storage.conversation[i].from == this.storage.conversation[i-1].from){
        return false
      }else{
        return true
      }
    }else{
      return true
    }
    
  }

  public sendMessage(message: NgForm) {
    //cannot send empty text unless whitespace is explicitly inserted
    if (message.valid) {
      var to = this.chatName
      var m = {
        to,
        from: this.global.activeUser,
        message: message.value.message,
        time: new Date().getTime(),
        viewed: false,
        received: false,
        hash: to+(new Date().getTime())
      }

      if(this.global.connectionStatus){
        this.socket.sendMessage(m)
      }

      this.storage.conversation.push(m)
      this.storage.updateLocalMessages([m])
      this.storage.updateChats(m.message, to)

      message.reset();
    }
  }

  /**
   * auto expand textarea on input,
   * textarea grows only till a certain size(rows)
   */
  expand(textarea) {
        //baseScroll property only comes after focus is set on textarea
        if (!textarea.baseScrollHeight) {
          var savedValue = textarea.value;
          textarea.value = '';
          textarea.baseScrollHeight = textarea.scrollHeight
          textarea.value = savedValue;
        }

          var minRows = textarea.getAttribute('data-min-rows') | 0
          var rows 
          textarea.rows = minRows;
          rows = Math.ceil((textarea.scrollHeight - textarea.baseScrollHeight) / 16)
          //text should not grow beyond five rows
          if(rows <= 4){
            textarea.rows = minRows + rows;
          }else{
            textarea.rows = 5
          }
  }

  get activeUser(){
    return this.global.activeUser
  }

  get conversation(){
    return this.storage.conversation
  }

}
