import { Component, OnInit } from '@angular/core';
import { slider, fader } from '../route-animations'
import { RouterOutlet } from '@angular/router';
import { SocketService } from './services/socket.service';
import { StorageService } from './services/storage.service';
import { ContactService } from './services/contact.service';
import { GlobalService } from '../core/services/global.service';

@Component({
  selector: 'app-workspace',
  templateUrl: './workspace.component.html',
  styleUrls: ['./workspace.component.scss'],
  animations: [fader, slider]
})
export class WorkspaceComponent implements OnInit {

  constructor(
    private socket: SocketService,
    private storage: StorageService,
    private contact: ContactService,
    private global: GlobalService
  ) { }

  ngOnInit() {
    //subscribe to new messages
    this.socket.newMessage
      .subscribe(obs => {

        //update chat area if user is already  in there
        if (this.storage.routeId == obs.from) {
          this.storage.conversation.push(obs)
        }

        //iterate over list of conversations
        for (var i = 0; i < this.storage.chats.length; i++) {

          //checks if you've already communicated with the user
          if (this.storage.chats[i].username == obs.from) {

            this.storage.chats[i].message = obs.message

            break
          }
        }

        this.storage.updateChats(obs.message, obs.from)
        this.storage.updateLocalMessages([obs])
      })

    this.socket.requests
      .subscribe(obs => {
        this.contact.updateRequests({ sent: false, username: obs })
      })

    this.socket.responses
      .subscribe((obs: any) => {
        var username = obs.username
        this.contact.removeFromAcquaintanceStore(username)
        if (obs.accept) {
          this.contact.updateContacts([username])
        } 
      })


  }

  getAnimationData(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }

}
