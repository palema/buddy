import { Component, OnInit } from '@angular/core';
import { GlobalService } from 'src/app/core/services/global.service';

@Component({
  selector: 'splash',
  templateUrl: './splash.component.html',
  styleUrls: ['./splash.component.scss']
})
export class SplashComponent implements OnInit {
  constructor(private global: GlobalService) { }

  ngOnInit() {
  }

  get splashVisible(){
    return this.global.splashVisible
  }

  get splashMessage(){
    return this.global.splashMessage
  }

}
