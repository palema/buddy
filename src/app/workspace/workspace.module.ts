import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms'


import { WorkspaceRoutingModule } from './workspace-routing.module';
import { WorkspaceComponent } from './workspace.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { GroupComponent } from './group/group.component';
import { ChatComponent } from './chat/chat.component';
import { SocketService } from './services/socket.service';
import { DiscoveryComponent } from './discovery/discovery.component';
import { SplashComponent } from './splash/splash.component';




@NgModule({
  declarations: [
    WorkspaceComponent,
    DashboardComponent,
    GroupComponent,
    ChatComponent,
    DiscoveryComponent,
    SplashComponent
  ],
  imports: [
    CommonModule,
    WorkspaceRoutingModule,
    FormsModule
  ],
  providers: [SocketService]
})
export class WorkspaceModule { }
