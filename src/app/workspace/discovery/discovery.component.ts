import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ContactService } from '../services/contact.service';
import { GlobalService } from 'src/app/core/services/global.service';
import { SocketService } from '../services/socket.service';

@Component({
  selector: 'discovery',
  templateUrl: './discovery.component.html',
  styleUrls: ['./discovery.component.scss']
})
export class DiscoveryComponent implements OnInit {
  public tabStatus = { discovery: true, contacts: false, acquaintances: false }
  public lastActive = 'discovery'

  constructor(
    private contact: ContactService,
    private global: GlobalService,
    private socket: SocketService
  ) { }

  ngOnInit() {
    this.fetchContacts('')
    this.contact.loading = true
    
  }

  searchFrom(form: NgForm){
    var query = form.value.query
    if(this.lastActive === 'discovery'){
      this.fetchContacts(query)
    }else {
      this.searchLocalStore(this.lastActive, query)
    }
  }

  fetchContacts(query: string) {
    this.contact.searchResults = []
    this.contact.findContacts(query)
      .subscribe((obs: any) => {
        obs.data.discover.forEach(element => {
          this.contact.fullSearchLocal('contacts', element.username)
            .then(res => {
              this.contact.loading = false
              if (res) {
                this.contact.searchResults.push({isContact: true, username: res.username})
              } else {
                this.contact.fullSearchLocal('acquaintances', element.username)
                  .then(res => {
                    if (res) {
                      if(res.sent){
                        this.contact.searchResults.push({sent: true, username: res.username })
                      }else{
                        this.contact.searchResults.push({received: true, username: res.username})
                      }
                    } else {
                      this.contact.searchResults.push({ username: element.username})
                    }
                  })
              }
            })
        })
      }, error=>{
        this.contact.loading = false
      })
  }

  searchLocalStore(store: string, contact?: string) {
    if (!contact) {
      this.contact.findAllLocal(store)
        .then(res => {
          this.contact.loading = false
          this.contact.searchResults = res
        })
    } else {
      this.contact.findByLocal(store, contact)
    }
  }

  /**
   * handles sending request to server and storing it in indexdb
   * @param username user getting the request
   * @param index position is search results array
   */
  sendRequest(username, index) {
    this.contact.searchResults[index] = { username, sent: true }
    this.socket.sendRequest({ to: username, from: this.global.activeUser })
    this.contact.updateRequests([{ sent: true, username }])
  }

  rejectRequest(username, index) {
    if(this.lastActive == 'acquaintances'){
      this.contact.searchResults.splice(index, 1)
    }else{
      this.contact.searchResults[index] = { username }
    }
    
    this.socket.sendResponse({ username, accept: false })
    this.contact.removeFromAcquaintanceStore(username)
  }

  acceptRequest(username, index) {
    if(this.lastActive == 'acquaintances'){
      this.contact.searchResults.splice(index, 1)
    }else{
      this.contact.searchResults[index] = { username, isContact: true }
    }

    
    this.socket.sendResponse({ username, accept: true })
    this.contact.updateContacts([username])
    this.contact.removeFromAcquaintanceStore(username)
  }

  cancelRequest(username, index) {
    if(this.lastActive == 'acquaintances'){
      this.contact.searchResults.splice(index, 1)
    }else{
      this.contact.searchResults[index] = { username }
    }
    
    this.socket.cancelRequest( username )
    this.contact.removeFromAcquaintanceStore(username)
  }

  isActiveUser(username) {
    return (this.global.activeUser == username)
  }

  clearSearch(searchForm: NgForm) {
    searchForm.reset();
  }

  switchTab(tab: string) {
    this.contact.loading = true
    this.contact.searchResults = []
    this.tabStatus[tab] = true
    this.tabStatus[this.lastActive] = false
    this.lastActive = tab

    if (tab === 'discovery') {
      this.fetchContacts('')

    } else if (tab === 'acquaintances' || tab === 'contacts') {
      this.searchLocalStore(tab)
    }

  }

  get searchResults(){
    return this.contact.searchResults
  }

  get loading(){
    return this.contact.loading
  }
}
