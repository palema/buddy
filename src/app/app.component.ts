import { Component, OnInit, OnDestroy } from '@angular/core'
import { RouterOutlet } from '@angular/router'
import { slider } from './route-animations'
import { Observable, fromEvent, Subscription } from 'rxjs'
import { GlobalService } from './core/services/global.service'
import { NgxIndexedDBService } from 'ngx-indexed-db'
import { SocketService } from './workspace/services/socket.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    slider
  ]
})
export class AppComponent implements OnInit, OnDestroy {

  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation']
  }

  private onlineEvent: Observable<Event>
  private offlineEvent: Observable<Event>
  private subscriptions: Subscription[] = []
  private authSubscription: Subscription

  constructor(private global: GlobalService, private indexDB: NgxIndexedDBService, private socket: SocketService) { }

  ngOnInit() {

    //detect if user has internet connection
    this.onlineEvent = fromEvent(window, 'online')
    this.offlineEvent = fromEvent(window, 'offline')

    this.subscriptions.push(this.onlineEvent.subscribe(e => {
      //authentication user on socket
      this.socket.authenticate(this.global.token)

      //wait for user to be authenticated before sending cached messages
      this.authSubscription = this.socket.authenticated.subscribe(obs => {
        //send all cached messages when user comes online
        this.indexDB.openCursor('offlineMessages', (evt) => {
          var cursor = (<any>evt.target).result;
          if (cursor) {
            this.socket.sendMessage(cursor.value)
            cursor.continue()
          }else{
            this.authSubscription.unsubscribe()
          }
        })
      })

      this.global.connectionStatus = true
    }))

    this.subscriptions.push(this.offlineEvent.subscribe(e => {
      this.global.connectionStatus = false
    }))

  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe())
  }


}
