import { Injectable } from '@angular/core'
import { CanActivate, CanActivateChild, CanLoad, Route, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router'
import { Observable, Subscription } from 'rxjs'
import { GlobalService } from '../core/services/global.service'
import { NgxIndexedDBService } from 'ngx-indexed-db'
import { SocketService } from '../workspace/services/socket.service'
import { StorageService } from '../workspace/services/storage.service'
import { ContactService } from '../workspace/services/contact.service'

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {
  allMessages$: Subscription
  allContacts$: Subscription
  allAcquaintances$: Subscription


  constructor(
    private router: Router,
    private global: GlobalService,
    private dbService: NgxIndexedDBService,
    private socket: SocketService,
    private storage: StorageService,
    private contact: ContactService
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.checkLogin(state.url)
  }
  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.canActivate(next, state)
  }
  
  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
    this.global.splashVisible = true
    return this.loadMessages().then(res => {
      this.global.splashVisible = false
      return res
    })
    .catch(err => {  return err })
  }

  /**
   * 
   * @param url route bieng accessed
   */
  checkLogin(url: string) {
    //check if global variable login is set 
    if (this.global.loggedIn) return true

    this.global.redirectUrl = url
    this.router.navigate(['/auth/login'])
    return false
  }

  loadMessages() {


    if (this.setupCredentials()) {

      this.loadAcquaintances()
      this.loadContacts()

      //get all messages of user and push them into messages array
      return this.dbService.count('messages')
        .then(messages => {
          if (messages > 0) {
            return true
          } else {

            //emit allMessages event from socket.io            
            this.socket.allMessages()

            this.allMessages$ = this.socket.messages.subscribe(obs => {
              this.storage.updateLocalMessages(obs)
              this.allMessages$.unsubscribe()
            }, error=>{
              this.allMessages$.unsubscribe()
            })
            return true
          }
        })
    }
  }

  loadContacts() {
    this.dbService.count('contacts')
      .then(contacts => {
        if (contacts > 0) {
          return
        } else {
          this.socket.getContacts()
          this.allContacts$ = this.socket.contacts.subscribe((obs: any) => {
            if (obs.contacts.length)
              this.contact.updateContacts(obs.contacts)

            this.allContacts$.unsubscribe()
          }, error=>{
            this.allContacts$.unsubscribe()
          })
        }
      })
  }

  loadAcquaintances() {
    this.dbService.count('acquaintances')
      .then(acq => {
        if (acq > 0) {
          return
        } else {
          this.socket.getAcquaintances()
          this.allAcquaintances$ = this.socket.acquaintances.subscribe((obs: any) => {

            if (obs) {
              var requests = []
              obs.requestSent.forEach(rs => {
                requests.push({ username: rs, sent: true })
              })

              obs.requestReceived.forEach(rr => {
                requests.push({ username: rr, sent: false })
              })


              if (requests.length)
                this.contact.updateRequests(requests)
            }
            this.allAcquaintances$.unsubscribe()
            
          }, error => {
            this.allAcquaintances$.unsubscribe()
          })

        }
      })
  }


  setupCredentials() {
    return this.dbService.getAll('details')
      .then((user: any) => {

        if (user.length == 1) {
          var lastLoggedIn = user[0]

          //no one is logged in
          if (!lastLoggedIn.loggedIn) return false

          //setup environment details
          this.global.activeUser = lastLoggedIn.username
          this.global.token = lastLoggedIn.token
          this.global.loggedIn = true

          //authenticate to socket.io
          this.socket.authenticate(lastLoggedIn.token)

          return true
        }
        return false
      })
  }
}
