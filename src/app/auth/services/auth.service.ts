import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { NgxIndexedDBService } from 'ngx-indexed-db';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private apollo: Apollo, private dbService: NgxIndexedDBService) {
  }

  private registerMutation = gql`
    mutation Register($username: String!, $password: String!){
      register(username:$username, password: $password){
        token,
        error
      }
    }
  `

  private loginQuery = gql`
    query Login($username:String!, $password: String!){
      login(username:$username, password:$password){
        token,
        error
      }
    }
  `


  public register(username: string, password: string) {
    return this.apollo.mutate<any>({
      mutation: this.registerMutation,
      variables: {
        username,
        password
      }
    })
  }

  public login(username: string, password: string) {
    return this.apollo.watchQuery<any>({
      query: this.loginQuery,
      variables: {
        username,
        password
      }
    }).valueChanges
  }


  cleanUp(username: string) {

    return this.dbService.clear('messages')
      .then(res => {
        return this.dbService.clear('acquaintances')
      })
      .then(res => {
        return this.dbService.clear('contacts')
      })
      .then(()=>{
        return this.dbService.clear('details')
      })
  }


}
