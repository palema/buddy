import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss', '../sharedAuth.component.scss']
})
export class RegisterComponent {
  public register$: Subscription
  registerResult: any;
  loading: boolean = false

  constructor(
    private router: Router,
    private auth: AuthService,
    private dbService: NgxIndexedDBService,
  ) { }


  public registerHandler(form: NgForm) {
    if(form.valid){
      this.loading = true
      this.register$ = this.auth.register(form.value.username, form.value.password)
      .subscribe(({ data }) => {
        if (data.register.error){
          this.loading = false
          this.registerResult = data.register.error
        }
        else {
          
          /** delete all data from local db */
          this.auth.cleanUp(form.value.username)
          .then(()=>{
            return this.dbService.add('details', { username: form.value.username, loggedIn: 1, token: data.register.token })
          })
          .then(res => {
            this.router.navigate(['/user/chats'])
          })
          .catch(err=>{
            this.registerResult = 'something went wrong'
          })

        }
      }, err => {
        this.loading = false
        this.registerResult = 'network error, try again later'
      })
    }

  }




}
