import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { GlobalService } from 'src/app/core/services/global.service';
import { Subscription } from 'rxjs';
import { NgxIndexedDBService } from 'ngx-indexed-db'
import { environment } from 'src/environments/environment'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss', '../sharedAuth.component.scss']
})
export class LoginComponent implements OnDestroy {
  ngOnDestroy(): void {
    //only unsubscribe login$ is set and not unsubscribed
    if (this.login$ && !this.login$.closed)
      this.login$.unsubscribe()
  }
  public loginResult: string
  public login$: Subscription
  public loading: boolean = false

  constructor(
    private auth: AuthService,
    private router: Router,
    private global: GlobalService,
    private dbService: NgxIndexedDBService
  ) { }

  public loginHandler(form: NgForm) {
    if (form.valid) {
      this.login$ = this.auth.login(form.value.username, form.value.password)
        .subscribe(({ data, loading }) => {
          this.loading = loading
          if (data.login.error) {
            this.loading = false
            this.loginResult = data.login.error
          } else {
            this.loading = true
            this.auth.cleanUp(form.value.username)
            .then(res => {
              this.dbService.add('details', { username: form.value.username, loggedIn: 1, token: data.login.token, lastLogin: new Date().getTime() })
                .then(res => {
                  if (this.global.redirectUrl)
                    this.router.navigate([this.global.redirectUrl])
                  else
                    this.router.navigate(['/user/chats'])
                })
            })
            .catch(err => {
              console.error('Error', err)
            })
          }
        }, err => {
          this.loading = false
          this.loginResult = 'network error, try again later'
        })
    }
  }


}
