import { Component } from "@angular/core";
import { slider } from '../route-animations'
import { RouterOutlet } from '@angular/router';


@Component({
    selector: 'auth',
    templateUrl: './auth.component.html',
    styleUrls: ['./auth.component.scss'],
    animations: [ slider ]
})

export class AuthComponent{
    prepareRoute(outlet: RouterOutlet){
        return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation']
      }

}