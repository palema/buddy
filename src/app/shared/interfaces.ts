export interface IMessageList{
    id: string,
    messages: [IMessage]
  }
  
  export interface IMessage{
    from: string,
    to: string,
    message: string, 
    time: number,
    viewed: boolean,
    received: boolean,
    hash: string
  }