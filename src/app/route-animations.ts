import {
    trigger,
    transition,
    style,
    query,
    group,
    animate
} from '@angular/animations'

const optional = { optional: true }


export const fader = trigger('fadeIn', [
    transition('chats <=> chat', [
        query(':enter, :leave', [
            style({
                position: 'absolute',
                left: 0,
                width: '100%',
                height: '100%',
                opacity: 0,
                transform: 'translateY(70%)'
            })
        ], { optional: true }),
        query(':enter', [
            animate('400ms ease',
                style({ opacity: 1, transform: 'scale(1) translateY(0)' })
            ),
        ], { optional: true })
    ]),
])


export const slider = trigger('slideIn', [
    transition('register => login', [
        style({ position: 'relative'}),
        query(':enter, :leave', [
            style({
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: '170px'
            })
        ], optional),
        query(':enter', [
            style({ 'left': '-100%' })
        ]),
        group([
            query(':leave', [
                animate('300ms ease-out', style({ 'left': '100%' }))
            ], optional),
            query(':enter', [
                animate('300ms ease-out', style({ 'left': '0%' }))
            ])
        ])
    ]),
    transition('login => register', [
        query(':enter, :leave', [
            style({
                position: 'absolute',
                top: 0,
                'right': 0,
                width: '100%',
                height: '170px'
            })
        ], optional),
        query(':enter', [
            style({ 'right': '-100%' })
        ]),
        group([
            query(':leave', [
                animate('600ms ease', style({ 'right': '100%' }))
            ], optional),
            query(':enter', [
                animate('600ms ease', style({ 'right': '0%' }))
            ])
        ])
    ])
])

export function slideTo(direction){
    return [
        query(':enter, :leave', [
            style({
                position: 'absolute',
                top: 0,
                [direction]: 0,
                width: '100%'
            })
        ], optional),
        query(':enter', [
            style({ [direction]: '-100%' })
        ]),
        group([
            query(':leave', [
                animate('600ms ease', style({ [direction]: '100%' }))
            ], optional),
            query(':enter', [
                animate('600ms ease', style({ [direction]: '0%' }))
            ])
        ])
    ]
}