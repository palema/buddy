import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

import { HttpClientModule } from '@angular/common/http'
import { GraphQLModule } from './graphql.module';
import { CoreModule } from './core/core.module'
import { ServiceWorkerModule } from '@angular/service-worker'
import { environment } from '../environments/environment'
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io'
import { NgxIndexedDBModule, DBConfig } from 'ngx-indexed-db'

const config: SocketIoConfig = { url: environment.production? window.location.origin: 'http://localhost:3456', options: { reconnection: false } }

const dbConfig: DBConfig = {
  name: 'Treasury',
  version: 3,
  objectStoresMeta: [
    {
      store: 'messages',
      storeConfig: { keyPath: 'id', autoIncrement: true },
      storeSchema: [
        { name: 'username', keypath: 'username', options: { unique: false } },
        { name: 'from', keypath: 'from', options: { unique: false } },
        { name: 'to', keypath: 'from', options: { unique: false } },
        { name: 'message', keypath: 'message', options: { unique: false } },
        { name: 'time', keypath: 'time', options: { unique: false } },
        { name: 'viewed', keypath: 'viewed', options: { unique: false } },
        { name: 'received', keypath: 'received', options: { unique: false } },
        { name: 'hash', keypath: 'hash', options: { unique: false } }
      ]
    },
    {
      store: 'chats',
      storeConfig: { keyPath: 'id', autoIncrement: true },
      storeSchema: [
        { name: 'username', keypath: 'username', options: { unique: true } },
        { name: 'message', keypath: 'message', options: { unique: false } }

      ]
    },
    {
      store: 'details',
      storeConfig: { keyPath: 'id', autoIncrement: true },
      storeSchema: [
        { name: 'username', keypath: 'username', options: { unique: true } },
        { name: 'token', keypath: 'token', options: { unique: true } },
        { name: 'loggedIn', keypath: 'loggedIn', options: { unique: false } },
        { name: 'lastLogin', keypath: 'lastLogin', options: { unique: false } }
      ]
    },
    {
      store: 'contacts',
      storeConfig: { keyPath: 'id', autoIncrement: true },
      storeSchema: [
        { name: 'username', keypath: 'username', options: { unique: true } }
      ]
    },
    {
      store: 'acquaintances',
      storeConfig: { keyPath: 'id', autoIncrement: true },
      storeSchema: [
        { name: 'username', keypath: 'username', options: { unique: true } },
        { name: 'sent', keypath: 'sent', options: { unique: false } }
      ]
    },
    {
      store: 'offlineMessages',
      storeConfig: { keyPath: 'id', autoIncrement: true },
      storeSchema: [
        { name: 'username', keypath: 'username', options: { unique: false } },
        { name: 'from', keypath: 'from', options: { unique: false } },
        { name: 'to', keypath: 'from', options: { unique: false } },
        { name: 'message', keypath: 'message', options: { unique: false } },
        { name: 'time', keypath: 'time', options: { unique: false } },
        { name: 'viewed', keypath: 'viewed', options: { unique: false } },
        { name: 'received', keypath: 'received', options: { unique: false } },
        { name: 'hash', keypath: 'hash', options: { unique: false } }
      ]
    },
    
  ]
}


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    GraphQLModule,
    HttpClientModule,
    CoreModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    NgxIndexedDBModule.forRoot(dbConfig),
    SocketIoModule.forRoot(config)
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }



