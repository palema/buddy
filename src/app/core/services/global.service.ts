import { Injectable } from '@angular/core';



@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  redirectUrl: string
  activeUser: string
  token: string 
  loggedIn: boolean = false
  splashVisible: boolean = false
  splashMessage: string
  connectionStatus: boolean = navigator.onLine
  

  constructor() { }  

}
