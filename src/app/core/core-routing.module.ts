import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../guard/auth.guard';


const routes: Routes = [
  { path: 'auth', loadChildren: 'src/app/auth/auth.module#AuthModule'},
  { path: 'user', canActivateChild:[AuthGuard], canLoad: [AuthGuard], loadChildren: 'src/app/workspace/workspace.module#WorkspaceModule'},
  { path: "", redirectTo: "/user/chats", pathMatch: "full"},
  { path: "**", redirectTo: "/user/chats", pathMatch: "full" }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule { }
